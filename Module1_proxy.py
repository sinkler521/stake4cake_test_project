from time import sleep
from selenium import webdriver
from selenium_stealth import stealth


def webdriver_with_stealth_and_options():
    """
    :return: modified chromedriver connected to a proxy
    important! check the proxy aviability. If it's unaviable you won't connect to a website
    """
    options = webdriver.ChromeOptions()

    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)

    # Proxy is from https://www.freeproxylists.net/ru/

    proxy = "93.114.194.26:1337"
    options.add_argument(f"--proxy-server={proxy}")
    options.add_argument('--incognito')

    #  webdriver
    driver = webdriver.Chrome(options=options, executable_path='webdriver/chromedriver.exe')

    stealth(
        driver=driver,
        user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.53 Safari/537.36',
        languages=["en-US", "en"],
        vendor="Google Inc.",
        platform="Win32",
        webgl_vendor="Intel Inc.",
        renderer="Intel Iris OpenGL Engine",
        fix_hairline=False,
        run_on_insecure_origins=False
    )

    return driver


def check_if_website_can_detect_webdriver():
    """
    This function is just a test. Usage is optional.
    While using Module1_proxy as a main, this function is being run.
    """

    driver = webdriver_with_stealth_and_options()

    driver.get('https://intoli.com/blog/not-possible-to-block-chrome-headless/chrome-headless-test.html')
    sleep(12)
    driver.quit()


if __name__ == '__main__':
    check_if_website_can_detect_webdriver()
