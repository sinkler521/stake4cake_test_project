import time
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from Module1_proxy import webdriver_with_stealth_and_options
from Module2_parse_data import get_data_from_sheets


class Logingoogleclass:
    """
    Google doesn't allow to log in directly, so we use the Stackoverflow to log in with Google
    """
    def __init__(self, current_user: int):
        self.url = 'https://stackoverflow.com/users/login'
        self.driver = webdriver_with_stealth_and_options()
        self.values = get_data_from_sheets()
        self.current_user = current_user

    def get_from_stackoverflow_to_google(self):
        self.driver.get(self.url)
        time.sleep(5)
        self.driver.find_element(
            By.XPATH, '//*[@id="openid-buttons"]/button[1]').click()  # click on "login with Google"

        time.sleep(3)

    def login(self):
        email, password = self.get_email_and_password()
        self.get_from_stackoverflow_to_google()

        self.driver.find_element(By.ID, 'identifierId').send_keys(email)  # find input field for email
        time.sleep(2)
        self.driver.find_element(
            By.XPATH, '//*[@id="identifierNext"]/div/button').click()  # press button for the next step
        time.sleep(5)

        try:
            self.driver.find_element(By.NAME, 'password').send_keys(password)
            time.sleep(2)
        except NoSuchElementException:  # if Google still doesn't allow to log in and "password" field isn't shown
            self.driver.find_element(By.ID, 'next').click()
            time.sleep(3)

    def get_email_and_password(self) -> tuple:
        """
        :return: tuple (email, password)
        """
        email, password = self.values[self.current_user][2], self.values[self.current_user][3]
        return email, password


if __name__ == '__main__':
    current_user = 1

    for each in range(len(get_data_from_sheets()) - 1):  # amount of users
        google_login = Logingoogleclass(current_user)
        google_login.login()

        current_user += 1  # every next iteration works with another (next) user
