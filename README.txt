libraries:
    pip install selenium selenium-stealth (for imitate user behavior)
    pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib (for parse data from Google sheets)

driver:
    Chromedriver version 108.0.5359.71

how to use:
    First of all, install those libraries required for modules correct usage. Check the connection (run Module1_proxy.py). If you are unable to connect and the webpage isn't being open,
    use another proxy (https://www.freeproxylists.net/ru/). Replace the old ip with a new one on Module1_proxy.py line 18.

    Then check if you are able to get data from Google sheets (run Module2_parse_data.py). It will print the data on console.

    Warning: Module 3 doesn't work correctly. Authorization to Google is impossible on this stage because Google treats the connection as insecure

